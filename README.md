# Node Authentication API

Backend authentication system API using Node.js

## Manual
Get the project and execute these command in the project diectory: 
```bash
npm install
```
```bash
npm start
```

## Routing
Add a new user
```bash
POST Rqeuest: HOST-ADDRESS:PORT-NUMBER/users/register
```
To check authentication, try
```bash
POST Request  HOST-ADDRESS:PORT-NUMBER/users/authenticate
```
A jwt token is retrieved on a successful authentication and to access the profile jwt is needed
```bash
GET Request: HOST-ADDRESS:PORT-NUMBER/users/profile
```


## Contribution

Anyone interested to contribute, is highly acceptable.

## License
    
The MIT License (MIT)

Copyright (c) 2017 Shams Nahid

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
