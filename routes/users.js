var express = require('express');
var router = express.Router();
var passport = require('passport');
var jwt = require('jsonwebtoken');
var config = require('../config/database');

var User = require('../models/user');

router.post('/register', (req, res, next) => {

    var newUser = new User({
        name: req.body.name,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    });

   User.addUser(newUser, (err, user) => {
        if(err) {
            console.log('Error in adding new user: ' + err);
            res.json({
                success: false,
                msg: 'Failed to add user: ' + err
            });
        } else {
            console.log('New user added: ' + user);
            res.json({
                success: true,
                msg: 'New user added: ' + user
            });
        }
    });
});

router.post('/authenticate', (req, res, next) => {
    var username = req.body.username;
    var password = req.body.password;

    User.getUserByName(username, (err, user) => {
       if(err) throw  err;
       if(!user) {
           return res.json({
               success: false,
               msg: 'user not found'
           });
       }

       User.comparePassword(password, user.password, (err, isMatch) => {
            if(err) throw err;
            if(isMatch) {
                var token = jwt.sign(user, config.secret, {
                    expiresIn: 604800 //1 week
                });
                res.json({
                    success: true,
                    token: 'JWT ' + token,
                    user: {
                        id: user._id,
                        name: user.name,
                        username: user.username,
                        email: user.email
                    }
                });
            } else {
                return res.json({
                    success: false,
                    msg: 'wrong password'
                });
            }
       });
    });
});

router.get('/profile', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    res.json({user: req.user})  ;
});

module.exports = router;