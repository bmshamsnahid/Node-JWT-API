var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var cors = require('cors');
var passport = require('passport');
var mongoose = require('mongoose');
var config = require('./config/database');

mongoose.connect(config.database, (err) => {
    if(err) {
        console.log('Error in database connection: ' + err);
    } else {
        console.log('Database connected successfully.');
    }
});

var app = express();
var users = require('./routes/users');

app.use(cors());

app.use(express.static(path.join(__dirname + 'public')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(passport.initialize());
app.use(passport.session());
var passportConfig = require('./config/passport')(passport);

var port = 3000;

app.use('/users', users);

app.get('/', (req, res) => {
    res.send('Invalid endpoint');
});

app.listen(port, () => {
    console.log('Server Running on port: ' + port);
});