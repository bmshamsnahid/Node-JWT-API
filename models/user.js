var mongoose  = require('mongoose');
var bcrypt = require('bcryptjs');
var config = require('../config/database');


var UserSchema = {
    name: {
        type: String
    },
    email: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
};

var user = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = function (id, callback) {
    user.findById(id, callback);
}

module.exports.getUserByName = function (username, callback) {
    var query = {username: username};
    user.findOne(query, callback);
}

module.exports.addUser = function (newUser, callback) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, function(err, hash) {
            if(err) throw err;
            newUser.password = hash;
            newUser.save(callback);
        });
    });
}

module.exports.comparePassword = function (candidatePassword, hash, callback) {
    bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
        if(err) throw err;
        return callback(null, isMatch);
    });
};